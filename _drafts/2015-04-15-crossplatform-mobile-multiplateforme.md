---
title: "Développement d'Application Mobile : Natif, Hybride ou Web ?"
categories: fr coding mobile android web
author: macha
---

Une application native est une app. développée sous le langage
spécifique à l'OS visé : par exemple, en kotlin pour Android, en Swift pour iOS
(en C/xaml pour Windows Phone). Elle peut alors être distribuée sur les boutiques
officielles App Store, Play Store ou Windows Phone Store.

**Hybride** **Web**

**Source:** [article sur le Développement d'application Mobile](http://www.grafikart.fr/blog/developper-application-mobile)
