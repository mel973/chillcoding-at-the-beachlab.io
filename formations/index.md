---
layout: blog
title: Formations | ChillCoding
---

# Formation Dev. Mobile : Développer une Application Native Android avec le langage Kotlin

{: style="text-align:center"}
![Android View](/assets/img/post/android-formation.png)

Cette formation s’adresse aux personnes scientifiques souhaitant se lancer
dans le développement d’application mobile sur _Android_ avec le langage _Kotlin_.
En particulier, les développeur·se·s ayant déjà une connaissance de la programmation
orientée objet ou fonctionnelle permettront d'aller plus loin dans ladite formation.

L’apprentissage se fait via des cours interactifs et des exercices pratiques
réalisés sous _Android Studio_. Des quizzs sont prévus pour consolider les notions abordées.

Tout au long des séances, afin d’augmenter sa productivité, il est indiqué
des astuces pour utiliser au mieux les outils de développement
(raccourcis clavier, auto-génération de code, bibliothèques, références, etc.).

De plus, les bonnes pratiques de développement seront mises en lumière.

A l'issue de la formation les participant·e·s sauront développer, tester et déployer une application mobile sur terminaux <i>Android</i>. Elle permettra de découvrir l’écosystème Android afin d’être autonome dans la réalisation d’applications de la phase de conception à la publication.

## Durée
3 à 17 jours

La formation est basée sur un tronc commun se déroulant sur 3 jours. Ensuite,
des jours optionnels peuvent être sélectionnés à la carte afin d'adapter
la formation aux besoins.

## Pré-requis
- Connaissance d'un langage de Programmation, Orientée Objet ou Fonctionnelle
- Connaissance de _Git_

## À préparer
* Ordinateur personnel
* Android Studio avec dernière version du SDK Android ([lien de téléchargement](https://developer.android.com/studio/index.html))
* Téléphone *Android*, avec câble de connexion

## Objectifs pédagogiques
* Configurer un environnement de développement *Android* à jour
* Comprendre et écrire du code *Kotlin*
* Connaître les principales bibliothèques du monde *Android*
* Savoir créer une interface graphique
* Développer une application *Android* en respectant les bonnes pratiques
* Travailler avec des bases de données et services web

## Plan
[Programme de formation détaillé (PDF)](https://www.chillcoding.com/assets/pdf/programme-formation-android-kotlin.pdf)

#### ABC d’Android
<ol type="A">
  <li>Plateforme <i>Android</i></li>
  <li>Environnement de développement</li>
  <li>Principes de programmation</li>
</ol>

Supports en ligne : [AK-1A: Android en bref], [AK-1B: Installation Android]

#### Pratique : Créer un premier projet 'Hello Android'
- Arborescence du projet, fichiers clés
- Déploiement sur émulateur et appareil physique
- Exploration du cycle de vie de l'*Activity*
- Gestion du clique sur une image (accès aux éléments de la vue)
<br>
<br>

#### Kotlin pour Android
<ol type="A">
 <li>Concepts du langage Kotlin</li>
 <li>Classe, propriété, fonction</li>
 <li>Variable, opérateur, condition</li>
 <li>Programmation fonctionnelle</li>
</ol>

Supports en ligne :[AK-2A: Kotlin en bref], [AK-2B: Vue personnalisée], [AK-2C: Tableau Kotlin]


#### Pratique : Créer une vue personnalisée
* Héritage et constructeurs
* Création d'objet via *data class*
* Initialisations de variables
* Manipulation des listes et tableaux
<br>
<br>

#### Interface Utilisateur Native
<ol type="A">
  <li><i>Material Design</i></li>
  <li>Vue et agencements</li>
  <li>Ressources (image vectorielle, thème, internationalisation, dimension, etc.)</li>
  <li>Composants graphiques natifs</li>
</ol>

Supports en ligne : [AK-4D: Bibliothèques Kotlin], [AK-4B: Plugin kotlin], [AK-4: UI Cheatsheet]

#### Pratique : Créer une interface utilisateur élaborée
* Manipulation du *ConstraintLayout*
* Mise en pratique des principaux composants graphiques (*CardView*)
* Gestion du mode portrait, paysage
* Ajout d’une langue
<br>
<br>

#### Menus
<ol type="A">
  <li>Navigation entre écrans</li>
  <li>Différents types de menu</li>
  <li>Notion de <i>Fragment</i></li>
  <li>Communication inter-composant</li>
</ol>

#### Pratique : Créer une application structurée
* Mise en pratique d'un des menus
<br>
<br>

#### Affichage d'une liste d'éléments
<ol type="A">
  <li>Principe d'adaptateur <i>Android</i></li>
  <li>Exemples de vue</li>
  <li>Implémentation du <i>RecyclerView</i></li>
</ol>

Support en ligne : [AK-6: RecyclerView]

#### Pratique : Afficher une liste d’élément
* Affichage de la liste des plus belles plages
<br>
<br>

#### Persistance des Données
<ol type="A">
  <li>Stockage de paires clé-valeur</li>
  <li>Système de fichiers</li>
  <li>Base de données (BDD) <i>SQLite</i></li>
  <li><i>Object Relationnel Mapping</i> (ORM) : Bibliothèque <i>Room</i></li>
</ol>

Supports en ligne : [AK-7: SharedPreferences], [AK-8: BDD]

#### Pratique : Manipuler une BDD dans un Thread parallèle
* Gestion d'une BDD locale
<br>
<br>

#### Communication Réseau
<ol type="A">
  <li>Contexte d’échange</li>
  <li>Traitement en tâche de fond, <i>Coroutines</i></li>
  <li>Communication HTTPS avec <i>Retrofit</i></li>
</ol>

Supports en ligne : [AK-9: Retrofit GET], [AK-9B: Retrofit POST]

#### Pratique : Consommer un Service Web distant
* Utilisation de la bibliothèque _Retrofit_
* Réception et envoie des données d’un service web
<br>
<br>

#### Outils pour Développer
<ol type="A">
  <li>Messages systèmes et console <i>Logcat</i></li>
  <li>Débogage via les points d’arrêt</li>
  <li>Déploiement et tests</li>
  <li>Bibliothèques et références</li>
</ol>

Supports en ligne : [AK-3D: Android References], [AK-3: Raccourcis clavier]

#### Pratique : Importer un projet exemple
* Exploration des projets exemples
* Analyse d'une application (mémoire, interface graphique, etc.)
* Compatibilité des versions et évolution
<br>
<br>

### Application de la formation :
{% include aside.html %}

### Pour aller plus loin

#### Réussir une Publication sur le PlayStore
Introduction à l’optimisation sur les boutiques en ligne d'application
(<i>App Store Optimization</i>, <i>ASO</i>), la console de publication, et les statistiques.
<ol type="A">
  <li>Publication</li>
  <li>Utilisateur</li>
  <li>Promotion</li>
</ol>

#### Multiplateforme avec Kotlin
<ol type="A">
  <li>Application multiplateforme native</li>
  <li>Concept de bibliothèque <i>Kotlin</i>, pour <i>Android</i> et <i>iOS</i></li>
  <li>Architecture de l'environnement de développement</li>
</ol>

#### Pratique : Créer un premier projet multiplateforme
* Configuration de l'environnement de développement
* Création d’une première bibliothèque partagée
<br>
<br>

#### Capteurs et Autres
<ol type="A">
  <li>Accéléromètre</li>
  <li>Son</li>
  <li>Utiliser d’autres applications comme la Camera</li>
</ol>

#### Pratique : Créer un jeu
* Animation graphiques et sonores d’éléments
* Utilisation de l’accéléromètre pour animer un élément graphique
* Prise d’une photo avec l’application native
* Ouverture de l’application des paramètres
<br>
<br>

#### Préférences d'un Utilisateur
<ol type="A">
  <li>Conception des préférences avec <i>Material Design</i></li>
  <li>Interfaces de préférences</li>
  <li>Fichiers de préférences</li>
  <li>Enregistrement et lecture via une classe <i>Kotlin</i> déléguée</li>
</ol>

Support en ligne : [AK-7: SharedPreferences]

#### Pratique : Enregistrer un profil utilisateur dans une Application
* Utilisation des préférences de l’utilisateur (clé-valeur)
* Création d’un écran de préférences
* Récupération des valeurs depuis le fichier de préférences
<br>
<br>

#### Géolocalisation et Cartographie
<ol type="A">
  <li>Géolocalisation</li>
  <li>Carte géographique avec l'<i>API Google Maps</i></li>
</ol>

#### Pratique : Afficher la localisation de l’utilisateur
* Utilisation des services _Google Maps_ dans une *Activity*
*  Affichage de la dernière position détectée
<br>
<br>

#### Montre Connectée avec Android Wear
<ol type="A">
  <li>Interface et ses différents modes</li>
  <li>Configuration d’un projet <i>Android Wear</i></li>
  <li>Communication des données entre les modules</li>
</ol>

#### Pratique : Créer un premier projet sur Android Wear
* Déploiement sur émulateur et sur montre
<br>
<br>

#### Pratique : Approfondissement d'un développement classique
* Ouverture d’un document PDF stocké en local
* Enregistrement d’une image dans un fichier
* Utilisation de *Fragment* avec *ViewPager* (menu à onglets)
* Communication d’information entre *Fragment*
<br>
<br>

#### La vérité sur l'affaire d'une application mobile
<ol>
  <li>Chemin d'une application mobile</li>
  <li>Phase de découverte</li>
  <li>Phase de conception</li>
  <li>Phase de concrétisation</li>
  <li>Phase de lancément</li>
  <li>Phase de maintenance</li>
  <li>Application mobile en chemin</li>
</ol>

#### Pratique : Créer une application mobile de A à Z
* Exploration du monde des applications
* Fonctionnement de l'application dont vous avez eu l'idée 
* Choix technologique éclairé
* Design de l'application (scénarios, tâches, arbres, maquettes)
* Mise en place du projet et développement
* Publications et tests utilisateurs
* Promotion et soumissions alternatives
* Maintenance
<br>
<br>

N'hésitez pas à contacter <a href="mailto:{{ site.data.members.macha.email }}" title="{{ site.data.members.macha.email }}">
  <i class="fa fa-envelope-o fa-x" data-wow-delay=".2s"></i> {{ site.data.members.macha.email }}
</a> pour un programme personnalisé.

[AK-1A: Android en bref]:https://www.chillcoding.com/blog/2014/08/08/extraordinaire-humanoide/
[AK-1B: Installation Android]:https://www.chillcoding.com/blog/2016/08/03/android-studio-installation
[AK-2A: Kotlin en bref]:https://www.chillcoding.com/blog/2017/07/11/android-kotlin-introduction/
[AK-2B: Vue personnalisée]:https://www.chillcoding.com/android-custom-view-heart/
[AK-2C: Tableau Kotlin]:https://www.chillcoding.com/blog/2019/09/26/kotlin-array/
[AK-3D: Android References]:https://www.chillcoding.com/blog/2017/01/27/android-references
[AK-3: Raccourcis clavier]:https://www.chillcoding.com/blog/2016/04/01/android-top-raccourcis/
[AK-4D: Bibliothèques Kotlin]:https://www.chillcoding.com/blog/2017/10/09/utiliser-anko-kotlin-android
[AK-4B: Plugin kotlin]:https://www.chillcoding.com/blog/2017/10/03/utiliser-extensions-kotlin/
[AK-4: UI Cheatsheet]:https://www.chillcoding.com/blog/2017/01/16/android-ui-cheatsheet/
[AK-6: RecyclerView]:https://www.chillcoding.com/blog/2018/10/22/creer-liste-recyclerview-kotlin-android/
[AK-7: SharedPreferences]:https://www.chillcoding.com/blog/2014/10/10/utiliser-fichier-preferences/
[AK-8: BDD]:https://www.chillcoding.com/blog/2018/01/17/creer-bdd-sqlite-kotlin-android/
[AK-9: Retrofit GET]:https://www.chillcoding.com/blog/2017/03/14/requete-http-get-retrofit-android/
[AK-9B: Retrofit POST]:https://www.chillcoding.com/blog/2015/11/16/requete-http-post-retrofit-android/
